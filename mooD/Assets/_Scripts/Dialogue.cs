﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

static class Constants
{
    //Definition of the background box
    public const int backgroundBoxX = 10;
    public static int backgroundBoxY = 2 * (Screen.height / 3);

    //Definition of the dialogue box
    public const int dialogueBoxX = 50;
    public static int dialogueBoxY = 2 * (Screen.height / 3);
    public static int dialogueBoxSize = Screen.width - 100;

    //Definition of the options' positions
    public static int firstOptionY = Constants.dialogueBoxY + 1 * 85;
    public static int secondOptionY = Constants.dialogueBoxY + 75 + 2 * 20;
    public static int thirdOptionY = Constants.dialogueBoxY + 75 + 3 * 20;
}

//Allow the passing of UnityEvents in the form of strings
[Serializable]
public class UnityEventWithString : UnityEvent<string>
{
}

public class Dialogue : MonoBehaviour
{
    //Receive the dialogue lines to show
    public List<DialogueLine> Lines = new List<DialogueLine>();

    //Check if the dialogue line is hidden
    public bool isHidden;

    //Allow "Continue" after this time
    public float DisableInteractionTimer;

    //Check how much time has passed since the dialogue was first shown
    private float _NotHiddenTime;

    //Handle the UnityEvents
    public string OnEndEventParameter;
    public UnityEventWithString  OnEndEvent;

    //Define the dialogue skipping key
    public string SkipDialogueKey;

    //Check if the dialogue has been skipped to prevent re-showing it
    public static bool SkippedDialog = false;

    //Check if the dialogue has finished
    private int _currentLineCounter = 0;

    //Used to set the current line
    private DialogueLine _currentLine;

    //Used to define the text's appearance
    private GUIStyle _labelStyle;

	// Use this for initialization
	void Start ()
	{
        _currentLine = Lines[_currentLineCounter];
	}
	
	// Update is called once per frame
	void Update ()
    {
        //Prevent input detection if the dialogue is hidden
        if (isHidden)
        {
            return;
        }
        else if (_NotHiddenTime == 0)
        {
            _NotHiddenTime = Time.time;
        }

        var interactable = false;
        if (Time.time - _NotHiddenTime > DisableInteractionTimer)
            interactable = true;

        //If the player presses the skip dialogue key, there is a dialogue after the current one and the current dialogue wasn't skipped
        if (Input.GetKeyUp(SkipDialogueKey) && _currentLine.Options.Count == 0 && Dialogue.SkippedDialog == false && interactable)
	    {
            Dialogue.SkippedDialog = true;
            _currentLineCounter++;
            return;
	    }
        
    }

    private void LateUpdate()
    {
        //Prevent line checking if the dialogue is hidden
        if (isHidden)
        {
            return;
        }

        //Check if the OnEndEvent should be called (if it exists)
        if (_currentLineCounter >= Lines.Count && OnEndEvent != null)
        {
            OnEndEvent.Invoke(OnEndEventParameter);
            isHidden = true;
            return;
        }
        else
        {
            _currentLine = Lines[_currentLineCounter];
        }
        var interactable = false;
        if (Time.time - _NotHiddenTime > DisableInteractionTimer)
        {
            interactable = true;
        }
        _currentLine.Update(interactable);
        Dialogue.SkippedDialog = false;
    }

    void OnGUI()
    {
        //Prevent drawing if the dialogue is hidden
        if (isHidden)
        {
            return;
        }

        //Define font used in the dialogue box
        if (_labelStyle == null)
        { 
            _labelStyle = GUI.skin.label;
            _labelStyle.font = Resources.Load<Font>("Fonts\\verdana");
        }
        _labelStyle.normal.textColor = Color.black;
        _labelStyle.fontSize = 13;

        if (_currentLine.ActorName == "Ruth Laner")
        {
            //Actual dialogue box
            _currentLine.DrawDialogueBox(new Rect(0, 0,
                                         Screen.width, Screen.height / 3),
                                         new Color(1f, 0f, 0f, 1));
        }
        else
        {
            //Actual dialogue box
            _currentLine.DrawDialogueBox(new Rect(0, Constants.backgroundBoxY,
                                         Screen.width, Screen.height / 3),
                                         new Color(0.75f, 0.75f, 0.75f, 1));
        }

        //Current line
        _currentLine.Draw();
    }

    public void DelayedDialogue()
    {
        Invoke("ActivateDialog", 7);
    }

    public void ActivateDialog()
    {
        //Find all dialogues
        var dialogs = GameObject.FindObjectsOfType<Dialogue>();

        //Hide all others except the current dialogue
        for(int i = 0; i < dialogs.Length; i++)
        {
            dialogs[i].isHidden = true;
        }
        isHidden = false;
    }
}

[Serializable]
public class DialogueLine
{
    public string ActorName;
    public string Text;
    public List<DialogueOption> Options = new List<DialogueOption>();

    public void DrawDialogueBox(Rect position, Color color)
    {
            //Creation of a super small texture that will allow the GUI Box to be filled instantly
            Texture2D boxBackground = new Texture2D(1, 1);

            //Sets first (and only rofl) pixel to specified colour
            boxBackground.SetPixel(0, 0, color);
            boxBackground.Apply();

            //Sets the GUI Box background to white
            GUI.skin.box.normal.background = boxBackground;

            //Create GUI Box at the given position with null string?????? idk how it actually worked, kinda random.
            GUI.Box(position, (string)null);
    }

    public void Draw()
    {
        //Accept "lnbrk" as a line break
        Text = Text.Replace("lnbrk", "\n");
        if (ActorName == "Ruth Laner")
        {
            GUI.Label(new Rect(Constants.dialogueBoxX, Constants.backgroundBoxX * 1.5f, 300, 80), "<b>" + ActorName + "</b>"); // Draw ActorName
            GUI.Label(new Rect(Constants.dialogueBoxX, Constants.backgroundBoxX * 3.5f, Constants.dialogueBoxSize, 200), Text); // Draw Dialogue Line
            GUI.Label(new Rect(Constants.dialogueBoxX, Screen.height - Constants.firstOptionY + 25, 400, 30),
                          "<b>Press F to Continue</b>");
        }
        else if (ActorName != "Ruth Laner")
        {
            GUI.Label(new Rect(Constants.dialogueBoxX, Constants.dialogueBoxY + 15, 300, 80), "<b>" + ActorName + "</b>"); // Draw ActorName
            GUI.Label(new Rect(Constants.dialogueBoxX, Constants.dialogueBoxY + 35, Constants.dialogueBoxSize, 200), Text); // Draw Dialogue Line
            GUI.Label(new Rect(Constants.dialogueBoxX, Constants.firstOptionY, 400, 30),
                          "<b>Press F to Continue</b>");
        }
    }

    public void Update(bool interactable)
    {
        foreach (var dialogueOption in Options)
        {
            dialogueOption.Update(interactable);
        }
    }
}

[Serializable]
public class DialogueOption
{
    public string Key;
    public string Text;
    public string EventValue;
    public UnityEventWithString OptionEvent;

    public void Draw(int OptionNumber)
    {
        if (OptionNumber == 0)
        {
            GUI.Label(new Rect(Constants.dialogueBoxX, Constants.firstOptionY, Constants.dialogueBoxSize, 30),
                      OptionNumber + 1 + " - " + Text);
        }
        else
        {
            GUI.Label(new Rect(Constants.dialogueBoxX, Constants.dialogueBoxY + 75 + (OptionNumber) * 20, Constants.dialogueBoxSize, 30),
                      OptionNumber + 1 + " - " + Text);
        }
    }

    public void Update(bool interactable)
    {
        if (Input.GetKeyUp(Key) && Dialogue.SkippedDialog == false && interactable)
        {
            OptionEvent.Invoke(EventValue);
        }
    }
}
