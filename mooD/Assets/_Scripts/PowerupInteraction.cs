﻿using UnityEngine;

public class PowerupInteraction : MonoBehaviour
{
    //The user interface
    private UserInterface _userInterface;

    private void Start()
    {
        //Instance creation
        _userInterface = GameObject.FindGameObjectWithTag("PlayerManager").GetComponent<UserInterface>();
    }

    private void OnTriggerEnter(Collider collision)
    {
        //If the player caught this powerup
        if(collision.gameObject.CompareTag("Player"))
        {
            //If this powerup is a health kit
            if (transform.GetChild(0).GetComponent<SpriteRenderer>().sprite.name == "Health_Kit")
            {
                //Increase the player's health
                collision.gameObject.GetComponent<PlayerGlobalVariables>().PlayerHP += 50;
                //Check if the increase changes the character's icon
                _userInterface.CheckIfChangeIcon();
                //Destroy this powerup
                Destroy(gameObject);
            }
            //If this powerup is an ammo crate
            else if (transform.GetChild(0).GetComponent<SpriteRenderer>().sprite.name == "Ammo")
            {
                //Increase the player's bullet count
                collision.gameObject.GetComponent<PlayerGlobalVariables>().Bullets += 10;
                //Destroy this powerup
                Destroy(gameObject);
            }
            //If this powerup is a trophy
            else if (transform.GetChild(0).GetComponent<SpriteRenderer>().sprite.name == "Wheelchair_Trophy")
            {
                //Increase the player's high score
                collision.gameObject.GetComponent<PlayerGlobalVariables>().HighScore += 3000;
                //Destroy this powerup
                Destroy(gameObject);
            }
        }
    }
}
