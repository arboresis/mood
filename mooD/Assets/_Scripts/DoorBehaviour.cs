﻿using System.Collections;
using UnityEngine;

public class DoorBehaviour : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        //Check if the player is present and open if they are
        if(other.CompareTag("Player"))
        {
            StartCoroutine("OpenDoor");
        }
    }

    private void OnTriggerExit(Collider other)
    {
        //Check if the player just exited and close if they did
        if (other.CompareTag("Player"))
        {
            StartCoroutine("CloseDoor");
        }
    }

    private IEnumerator OpenDoor()
    {
        //Move vertically until this door is below ground
        while (transform.localPosition.y >= -0.6f)
        {
            transform.position -= new Vector3(0, 0.1f, 0);
            yield return new WaitForSeconds(0.01f);
        }
    }

    private IEnumerator CloseDoor()
    {
        //Move vertically until this door is back to its original position
        while (transform.localPosition.y <= 0.4f)
        {
            transform.position += new Vector3(0, 0.1f, 0);
            yield return new WaitForSeconds(0.01f);
        }
    }
}
