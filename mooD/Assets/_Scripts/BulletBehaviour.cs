﻿using System.Collections;
using UnityEngine;

public class BulletBehaviour : MonoBehaviour
{
    //The bullet's speed
    public float _bulletSpeed = 20f;

    //The bullet's Rigidbody
    private Rigidbody _rigidbody;
    //The bullet's relative velocity
    private Vector3 _relativeVelocity;

    //The player's offset when firing a bullet
    private GameObject _gunFiringOffset;
    //The muzzle flash
    public GameObject Explosion;
    private GameObject instance;

    private void Start()
    {
        //Instance creation
        _rigidbody = GetComponent<Rigidbody>();
        _gunFiringOffset = GameObject.FindGameObjectWithTag("FiringOffset");
        instance = Instantiate(Explosion, transform.position, transform.rotation);
        transform.Rotate(90f, 0, 0);

        //Start a coroutine to self-destruct if it doesn't hit anything
        StartCoroutine(SelfDestroy());
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //Add a forward force to the (rotated) bullet
        _rigidbody.AddForce(transform.up * _bulletSpeed);
    }

    private void OnCollisionEnter(Collision collision)
    {
        //If the target hit was an enemy, damage it
        if (collision.gameObject.CompareTag("Enemy") || collision.gameObject.CompareTag("OldManEnemy"))
        {
            collision.gameObject.GetComponent<EnemyBehaviour>().TakeDamage();
        }

        //Destroy the bullet
        Destroy(gameObject);
    }

    private IEnumerator SelfDestroy()
    {
        yield return new WaitForSeconds(0.05f);
        Destroy(instance);
        yield return new WaitForSeconds(5f);
        Destroy(gameObject);
    }
}
