﻿using UnityEngine;

public class RandomSprite : LookAtBehaviour
{
    //Receive two sprites
    public Sprite[] Sprites = new Sprite[2];

    private void Start()
    {
        //Instance creation
        _playerTransform = GameObject.FindGameObjectWithTag("Player").transform;

        //Randomise the sprite selection
        float sprite = Random.Range(0f, 1f);

        //Set the sprite according to the random sprite generated above
        if (sprite < 0.5f)
        {
            GetComponent<SpriteRenderer>().sprite = Sprites[0];
        }
        else
        {
            GetComponent<SpriteRenderer>().sprite = Sprites[1];
        }
    }
}
