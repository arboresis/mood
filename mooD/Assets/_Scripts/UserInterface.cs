﻿using UnityEngine;
using UnityEngine.UI;

public class UserInterface : MonoBehaviour
{
    //The player's HP bar
    public GameObject HPBar;
    //The player's variables script
    private PlayerGlobalVariables _playerBehaviour;

    private Text
        //The ammo text
        _ammoNumber,
        //The high score text
        _highScoreNumber;

    //VJ Blasterkiss's health-dependent icons
    public Sprite[] VJFaces = new Sprite[4];

    //The icon container
    private SpriteRenderer _vjFaceContainer;

    private void Start()
    {
        //Instance creation
        _playerBehaviour = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerGlobalVariables>();
        _ammoNumber = GameObject.FindGameObjectWithTag("UIAmmo").GetComponent<Text>();
        _highScoreNumber = GameObject.FindGameObjectWithTag("UIHighScore").GetComponent<Text>();
        _vjFaceContainer = GameObject.FindGameObjectWithTag("UIVJIcon").GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void LateUpdate()
    {
        //Set the HP bar's scale to match the player's HP
        HPBar.transform.localScale = new Vector3(
                                                 HPBar.transform.localScale.x,
                                                 (float) _playerBehaviour.PlayerHP / 100,
                                                 HPBar.transform.localScale.z);

        //Set the ammo text to match the player's bullet count
        _ammoNumber.text = _playerBehaviour.Bullets.ToString();

        //Set the high score text to match the player's high score
        _highScoreNumber.text = _playerBehaviour.HighScore.ToString();
    }

    public void CheckIfChangeIcon()
    {
        //Change the character's icon according to the HP level
        if(_playerBehaviour.PlayerHP <= 20)
        {
            _vjFaceContainer.sprite = VJFaces[0];
        }
        else if (_playerBehaviour.PlayerHP <= 40)
        {
            _vjFaceContainer.sprite = VJFaces[1];
        }
        else if (_playerBehaviour.PlayerHP <= 70)
        {
            _vjFaceContainer.sprite = VJFaces[2];
        }
        else if (_playerBehaviour.PlayerHP > 70)
        {
            _vjFaceContainer.sprite = VJFaces[3];
        }
    }
}
