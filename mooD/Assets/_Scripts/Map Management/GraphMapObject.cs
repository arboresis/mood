﻿using UnityEngine;

[RequireComponent(typeof(AbstractGraph))]
public class GraphMapObject : MonoBehaviour
{
    //The node prefabs (to instantiate according to the node type)
    public GameObject[] NodePrefabs = new GameObject[7];

    public void Initialization(AbstractGraph graph)
    {
        foreach (AbstractNode n in graph.Nodes)
        {
            GameObject instance;

            switch (n.type)
            {
                //If the node is floor
                case NodeType.Floor:
                    break;
                //If the node is a wall
                case NodeType.Wall:
                    instance = Instantiate(NodePrefabs[0], n.NodePosition, Quaternion.identity);
                    instance.transform.SetParent(GameObject.FindGameObjectWithTag("Map").transform);
                    instance.tag = "Wall";
                    break;
                //If the node is a door, check its neighbour nodes to see which direction it should face
                case NodeType.Door:
                    if(graph.GetNextNode(n).type != NodeType.Wall)
                    {
                        instance = Instantiate(NodePrefabs[1], n.NodePosition, Quaternion.identity);
                        instance.transform.Rotate(0, 90f, 0);
                    }
                    else
                    {
                        instance = Instantiate(NodePrefabs[1], n.NodePosition, Quaternion.identity);
                    }
                    instance.transform.SetParent(GameObject.FindGameObjectWithTag("Map").transform);
                    instance.tag = "Door";
                    //Disable the door interaction if the door is at the beginning of the level
                    if(n.y == 0)
                    {
                        instance.transform.GetChild(0).GetComponent<DoorInteractionScript>().enabled = false;
                    }
                    break;
                //If the node is a spawn door, check its neighbour nodes to see which direction it should face
                case NodeType.SpawnDoor:
                    if (graph.GetNextNode(n).type != NodeType.Wall)
                    {
                            instance = Instantiate(NodePrefabs[2], n.NodePosition, Quaternion.identity);
                            instance.transform.Rotate(0, -90f, 0);
                    }
                    else
                    {
                        if (graph.GetPreviousNode(n).type == NodeType.Wall)
                        {
                            instance = Instantiate(NodePrefabs[2], n.NodePosition, Quaternion.identity);
                        }
                        else
                        {
                            instance = Instantiate(NodePrefabs[2], n.NodePosition, Quaternion.identity);
                            instance.transform.Rotate(0, -90f, 0);
                        }
                    }
                    instance.transform.SetParent(GameObject.FindGameObjectWithTag("Map").transform);
                    instance.tag = "SpawnDoor";
                    break;
                //If the node is a powerup
                case NodeType.Powerup:
                    instance = Instantiate(NodePrefabs[3], n.NodePosition, Quaternion.identity);
                    instance.transform.SetParent(GameObject.FindGameObjectWithTag("Map").transform);
                    instance.tag = "Powerup";
                    break;
                //If the node is a cover obstacle
                case NodeType.Cover:
                    instance = Instantiate(NodePrefabs[4], n.NodePosition, Quaternion.identity);
                    instance.transform.SetParent(GameObject.FindGameObjectWithTag("Map").transform);
                    instance.tag = "Cover";
                    break;
                //If the node is a relic
                case NodeType.Relic:
                    instance = Instantiate(NodePrefabs[5], n.NodePosition, Quaternion.identity);
                    instance.transform.SetParent(GameObject.FindGameObjectWithTag("Map").transform);
                    instance.tag = "Relic";
                    break;
                //If the node is a lamp
                case NodeType.Lamp:
                    instance = Instantiate(NodePrefabs[6], n.NodePosition, Quaternion.identity);
                    instance.transform.SetParent(GameObject.FindGameObjectWithTag("Map").transform);
                    instance.tag = "Lamp";
                    break;
                //If the node is a fake wall, check its neighbour nodes to see which direction it should face
                case NodeType.FakeWall:
                    if (graph.GetNextNode(n).type != NodeType.Wall)
                    {
                        instance = Instantiate(NodePrefabs[7], n.NodePosition, Quaternion.identity);
                        instance.transform.Rotate(0, 90f, 0);
                    }
                    else
                    {
                        instance = Instantiate(NodePrefabs[7], n.NodePosition, Quaternion.identity);
                        
                        
                    }
                    instance.transform.SetParent(GameObject.FindGameObjectWithTag("Map").transform);
                    instance.tag = "FakeWall";
                    break;
                //If the node is the end-level node
                case NodeType.EndLevel:
                    instance = Instantiate(NodePrefabs[8], n.NodePosition, Quaternion.identity);
                    instance.transform.SetParent(GameObject.FindGameObjectWithTag("Map").transform);
                    instance.tag = "EndLevel";
                    break;
            }
        }
    }
}



