﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    //The spawn position
    private Vector3 _spawnPosition;

    //The object to instantiate if the player kills an enemy
    public GameObject Anderson;

    // Start is called before the first frame update
    void Start()
    {
        //Value definition
        _spawnPosition = transform.position;
    }

    public void SpawnEnemy()
    {
        //Start the open door/spawn enemy/close door coroutine
        StartCoroutine("SpawnEnemyAnimation");
    }

    IEnumerator SpawnEnemyAnimation()
    {
        //Open the door
        GetComponent<Animator>().SetBool("Open", true);

        yield return new WaitForSeconds(0.01f);

        //Close the door
        GetComponent<Animator>().SetBool("Open", false);

        //Instantiate an enemy at this position
        Instantiate(Anderson, transform.position, Quaternion.identity);
    }
}
