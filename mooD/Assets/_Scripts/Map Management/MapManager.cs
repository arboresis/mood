﻿using UnityEngine;

public class MapManager : MonoBehaviour
{
    //The map file reader for the terrain
    private MapFileReader TerrainMap;
    //The abstract graph (to generate the actual map)
    private AbstractGraph Graph;

    // Start is called before the first frame update
    void Start()
    {
        //Instance creation
        TerrainMap = GameObject.FindGameObjectWithTag("TerrainMap").GetComponent<MapFileReader>();
        Graph = GameObject.FindGameObjectWithTag("GraphContainer").GetComponent<AbstractGraph>();

        //Create a new map
        int[,] Map = TerrainMap.FileToArray();
        //Initialize the node graph with the map received from the text file
        Graph.GraphInitialization(Map);

        //Initialise the map with the objects corresponding to every node
        GraphMapObject graphMapObject = Graph.gameObject.GetComponent<GraphMapObject>();
        graphMapObject.Initialization(Graph);
    }
}
