﻿using UnityEngine;

public class AbstractGraph : MonoBehaviour
{
    //The dimensions of the graph (map)
    public int
        VerticalDimensions,
        HorizontalDimensions;

    //The array which will take in the nodes
    public AbstractNode[,] Nodes;

    public void GraphInitialization(int[,] terrainMap)
    {
        //Get the yy size of the terrain map
        VerticalDimensions = terrainMap.GetLength(1);
        //Get the xx size of the terrain map
        HorizontalDimensions = terrainMap.GetLength(0);

        //Create a new abstract node array with the desired dimensions
        Nodes = new AbstractNode[HorizontalDimensions, VerticalDimensions];

        //For each map node, instantiate the node at its specified position
        for (int y = 0; y < VerticalDimensions; y++)
        {
            for (int x = 0; x < HorizontalDimensions; x++)
            {
                //Read the node's type from the text file's (already read and "translated") array
                NodeType type = (NodeType) terrainMap[x, y];

                //Create a new abstract node at a specified position with a specified type
                AbstractNode newNode = new AbstractNode(x, y, type);

                //Set its node position values to its actual position
                newNode.NodePosition = new Vector3(x, 0, y);

                //Define the current node as the newborn node
                Nodes[x, y] = newNode;
            }
        }
    }

    //Get the current node
    public AbstractNode GetNode(int x, int y)
    {
        return Nodes[x, y];
    }

    //Get the next horizontal node
    public AbstractNode GetNextNode(AbstractNode Node)
    {
        return Nodes[Node.x + 1, Node.y];
    }

    //Get the previous horizontal node
    public AbstractNode GetPreviousNode(AbstractNode Node)
    {
        return Nodes[Node.x - 1, Node.y];
    }

    //Get the next vertical node
    public AbstractNode GetNextYNode(AbstractNode Node)
    {
        return Nodes[Node.x, Node.y + 1];
    }

    //Get the previous vertical node
    public AbstractNode GetPreviousYNode(AbstractNode Node)
    {
        return Nodes[Node.x, Node.y - 1];
    }
}
