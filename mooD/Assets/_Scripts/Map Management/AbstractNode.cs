﻿using UnityEngine;

//Each node's type number
public enum NodeType
{
    Floor = 0,
    Wall = 1,
    Door = 2,
    SpawnDoor = 3,
    Powerup = 4,
    Cover = 5,
    Relic = 6,
    Lamp = 7,
    FakeWall = 8,
    EndLevel = 9
};

public class AbstractNode
{
    //Set the default type to floor
    public NodeType type = NodeType.Floor;

    //Set the default position values
    public int
        x = -1,
        y = -1;

    //The node's position in three-dimensional space
    public Vector3 NodePosition;

    //Create a new abstract node at the specified position with a specified type
    public AbstractNode(int xIndex, int yIndex, NodeType nodeType)
    {
        x = xIndex;
        y = yIndex;
        type = nodeType;
    }
}
