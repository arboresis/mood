﻿using UnityEngine;

public class NodeObject : MonoBehaviour
{
    //The node's prefab
    public GameObject Prefab;

    public void Init(AbstractNode node)
    {
        //Set the position as the node's position
        transform.position = node.NodePosition;
        //Reset the scale to 1
        Prefab.transform.localScale = new Vector3(1f, 1f, 1f);
    }
}
