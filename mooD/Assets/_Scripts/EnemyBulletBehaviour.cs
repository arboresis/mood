﻿using UnityEngine;

public class EnemyBulletBehaviour : BulletBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        //Check if the target hit was the player and damage them if it was
        if(collision.gameObject.CompareTag("Player"))
        {
            GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerGlobalVariables>().ReceiveBaseDamage();
        }
    }
}
