﻿using System.Collections;
using UnityEngine;

public class OldManFiring : OldManBase
{
    //The bullet fired
    public GameObject Bullet;

    //The time at which this unit began firing
    private float _timeBeganFiring;
    //The firing spots (in mecha's case, the two weapons)
    public Vector3[] FiringPoints;
    //Firing rate
    public float SecondsBeforeFiring = 0.1f;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
    {
        //Instance creation
        _This = animator.gameObject.transform;
        _Player = GameObject.FindGameObjectWithTag("Player").transform;
        _Graph = GameObject.FindGameObjectWithTag("GraphContainer").GetComponent<AbstractGraph>();
        FiringPoints = new Vector3[2];        
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
    {
        //Update the firing points' position
        FiringPoints[0] = _This.GetChild(0).GetChild(0).position;
        FiringPoints[1] = _This.GetChild(0).GetChild(1).position;

        //If the time elapsed is bigger than the fire rate, fire
        if (Time.time > _timeBeganFiring)
        {
            _timeBeganFiring = Time.time + SecondsBeforeFiring;
            StartFiringLoop();
        }
    }

    public void StartFiringLoop()
    {
        Debug.Log("Firing");

        //Choose a random weapon
        float Weapon = Random.Range(0f, 1f);

        //Fire a bullet from the selected random weapon
        if (Weapon < 0.5f)
        {
            Instantiate(Bullet, FiringPoints[0], _This.GetChild(0).rotation);
        }
        else
        {
            Instantiate(Bullet, FiringPoints[1], _This.GetChild(0).rotation);
        }
    }
}