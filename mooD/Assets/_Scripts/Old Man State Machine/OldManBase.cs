﻿using UnityEngine;

public class OldManBase : StateMachineBehaviour
{
    protected Transform
        //This Transform
        _This,
        //The player's transform
        _Player;

    //The abstract graph containing each node
    protected AbstractGraph _Graph;

    //The target position to which this unit will move
    protected Vector3 _TargetPosition;

    //The speed at which this unit moves
    public float Speed = 2.0f;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
    {
        //Instance creation
        _This = animator.gameObject.transform;
        _Player = GameObject.FindGameObjectWithTag("Player").transform;
        _Graph =  GameObject.FindGameObjectWithTag("GraphContainer").GetComponent<AbstractGraph>();
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
    {
        
    }
}
