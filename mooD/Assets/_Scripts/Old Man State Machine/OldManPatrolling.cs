﻿using UnityEngine;

public class OldManPatrolling : OldManBase
{
    //The time at which the new waypoint began
    private float _timeBeganPatrolling;
    //The waypoint towards which the enemy will go
    private Vector3 _waypoint;
    //The time before the waypoint is recalculated
    public int SecondsBeforeWayPointChanges = 3;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
    {
        //Instance creation
        _This = animator.gameObject.transform;
        _Player = GameObject.FindGameObjectWithTag("Player").transform;
        _Graph = GameObject.FindGameObjectWithTag("GraphContainer").GetComponent<AbstractGraph>();

        //Start patrolling
        StartPatrolLoop(_This.gameObject);
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
    {
        //Move towards the waypoint (relative to position)
        _This.GetComponent<Rigidbody>().MovePosition(_This.position + (_waypoint * 0.05f));

        //If the time elapsed is bigger than the new waypoint calculation threshold
        if (Time.time > _timeBeganPatrolling)
        {
            //Calculate a new waypoint
            _timeBeganPatrolling = Time.time + SecondsBeforeWayPointChanges;
            StartPatrolLoop(_This.gameObject);
        }
    }

    public void StartPatrolLoop(GameObject _oldMan)
    {
        //Used to get the current node
        AbstractNode _thisNode;

        //Used to get the values for the new waypoint
        float
            _waypointX = 0,
            _waypointZ = 0;

        //Used to round the positional values to integers, because casting does not round as expected
        int
            _nodeX = Mathf.RoundToInt(_This.position.x),
            _nodeY = Mathf.RoundToInt(_This.position.z);

        //Get the current node
        _thisNode = _Graph.GetNode(_nodeX, _nodeY);

        //Check the graph for walls and change the random values accordingly
        if (_Graph.GetPreviousNode(_thisNode).type != NodeType.Floor)
        {
            _waypointX = Random.Range(0f, 1f);
        }
        if (_Graph.GetNextNode(_thisNode).type != NodeType.Floor)
        {
            _waypointX = Random.Range(-1f, 0f);
        }
        if (_Graph.GetPreviousYNode(_thisNode).type != NodeType.Floor)
        {
            _waypointZ = Random.Range(0f, 1f);
        }
        if (_Graph.GetNextYNode(_thisNode).type != NodeType.Floor)
        {
            _waypointZ = Random.Range(-1f, 0f);
        }

        //If all the surrounding nodes are free, the random values vary freely
        if (_Graph.GetPreviousNode(_thisNode).type == NodeType.Floor)
        {
            if (_Graph.GetNextNode(_thisNode).type == NodeType.Floor)
            {
                _waypointX = Random.Range(-1f, 1f);
            }
        }
        if (_Graph.GetPreviousYNode(_thisNode).type == NodeType.Floor)
        {
            if (_Graph.GetNextYNode(_thisNode).type == NodeType.Floor)
            {
                _waypointZ = Random.Range(-1f, 1f);
            }
        }

        //Define the waypoint
        _waypoint = new Vector3(_waypointX,
                                0,
                                _waypointZ);
    }
}
