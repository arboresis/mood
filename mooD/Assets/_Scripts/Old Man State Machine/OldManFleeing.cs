﻿using UnityEngine;

public class OldManFleeing : OldManBase
{
    public override void OnStateEnter(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
    {
        //Instance creation
        _This = animator.gameObject.transform;
        _Player = GameObject.FindGameObjectWithTag("Player").transform;
        _Graph = GameObject.FindGameObjectWithTag("GraphContainer").GetComponent<AbstractGraph>();
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
    {
        //Update the target position; an enemy will flee from the player if the player gets too close to it
        _TargetPosition = _This.position - _Player.position;
        _TargetPosition = new Vector3(_TargetPosition.x, 0, _TargetPosition.z);
        _TargetPosition.Normalize();

        //Move in the opposite direction from which the player is approaching
        _This.GetComponent<Rigidbody>().MovePosition(_This.position + _TargetPosition * 0.1f);
    }
}
