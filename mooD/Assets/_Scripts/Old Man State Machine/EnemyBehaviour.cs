﻿using UnityEngine;

public class EnemyBehaviour : MonoBehaviour
{
    //The enemy's animator
    private Animator _animator;

    //The player's Transform
    private Transform _player;

    //The state machine's patrolling behaviour
    private OldManPatrolling _oldManPatrolling;

    //This enemy's hp
    public int EnemyHP;
    
    // Start is called before the first frame update
    void Start()
    {
        //Instance creation
        _animator = GetComponent<Animator>();
        _player = GameObject.FindGameObjectWithTag("Player").transform;

        //Value definition
        EnemyHP = 100;
    }

    // Update is called once per frame
    void Update()
    {
        //Update the distance parameter
        _animator.SetFloat("DistanceToPlayer", Vector3.Distance(transform.position, _player.position));
        //Reset the yy position to 0
        transform.position = new Vector3(transform.position.x, 0, transform.position.z);
    }

    public void TakeDamage()
    {
        //Randomise the damage taken (which makes it between two and three hits to kill an enemy)
        EnemyHP -= (int)Random.Range(35f, 55f);

        //If the enemy dies
        if(EnemyHP <= 0)
        {
            //Create an empty variable which will take the target spawn point
            GameObject _spawnPoint;
            //Get all the available spawn points
            GameObject[] _possibleSpawnPositions = GameObject.FindGameObjectsWithTag("EnemySpawnDoor");

            //Always makes sure there is a comparable object
            _spawnPoint = _possibleSpawnPositions[0];

            foreach(GameObject PossibleSpawnPoint in _possibleSpawnPositions)
            {
                //If the distance to this spawn point is less than the distance to the target spawn point
                if(Vector3.Distance(transform.position, PossibleSpawnPoint.transform.position) < Vector3.Distance(transform.position, _spawnPoint.transform.position))
                {
                    //Update the target spawn point to the current one
                    _spawnPoint = PossibleSpawnPoint;
                }
            }

            //Increase the player's high score
            _player.gameObject.GetComponent<PlayerGlobalVariables>().HighScore += 500;

            //Spawn an enemy at the target spawn point
            _spawnPoint.GetComponent<EnemySpawner>().SpawnEnemy();

            //If the enemy killed was an unarmed old man, remove him from the old man list
            if(gameObject.CompareTag("OldManEnemy"))
            {
                GameObject.FindGameObjectWithTag("EnemyManager").GetComponent<AndersonConnection>().OldManList.Remove(gameObject);
            }
            //If the enemy killed was a mecha, fetch its protection target (and set it to null) and set it as unprotected
            if(gameObject.CompareTag("Enemy"))
            {
                if(GetComponent<AndersonProtector>().ProtectionTarget != null)
                {
                    GetComponent<AndersonProtector>().ProtectionTarget.GetComponent<AndersonProtected>().IsProtected = false;
                    GetComponent<AndersonProtector>().ProtectionTarget = null;
                }
            }

            //Destroy this enemy
            Destroy(gameObject);
        }
    }
}
