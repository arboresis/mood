﻿using UnityEngine;

public class TimerDestroyer : MonoBehaviour
{
    public float DeathTimer = 0.05f;

    private float _currentTime = 0.0f;

    //The firing offset
    private Transform FiringOffset;

    private void Awake()
    {
        //Instance creation
        FiringOffset = GameObject.FindGameObjectWithTag("FiringOffset").transform;

        //Define position
        transform.position = FiringOffset.position;
    }

    private void Start()
    {
        //Define position
        transform.position = FiringOffset.position;
    }

    private void Update()
    {
        transform.position = FiringOffset.position;
        if (_currentTime < 0.05f)
        {
            _currentTime = Time.time + DeathTimer;
        }
        else
        {
            Destroy(gameObject);
        }
    }
}
