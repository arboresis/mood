﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Video;

public class PlayerShootingHandler : MonoBehaviour
{
    //The player character's global variables
    private PlayerGlobalVariables _globalVariables;

    //The firing position offset
    private Transform FiringOffset;

    private float _nextFire = 0.0f;

    public GameObject Explosion;

    // Use this for initialization
    void Start()
    {
        //Instance creation
        FiringOffset = GameObject.FindGameObjectWithTag("FiringOffset").transform;
        _globalVariables = GetComponent<PlayerGlobalVariables>();
    }

    private void Update()
    {
        if (_globalVariables.playerShouldMove)
        {
            //Firing handle
            if (Input.GetMouseButtonDown(0) && Time.time > _nextFire)
            {
                //If the player has bullets
                if (_globalVariables.Bullets != 0)
                {
                    _nextFire = Time.time + _globalVariables.FireRate;

                    //The object hit
                    RaycastHit hit;
                    //If the shot hit something
                    if (Physics.Raycast(FiringOffset.position, transform.TransformDirection(Vector3.forward), out hit, Mathf.Infinity))
                    {
                        //Damage the target
                        hit.collider.SendMessage("TakeDamage", SendMessageOptions.DontRequireReceiver);
                    }
                    //Instantiate a muzzle flash on the player's weapon
                    Instantiate(Explosion, FiringOffset.position, transform.rotation);
                    //Decrease the bullet count
                    _globalVariables.Bullets--;
                }
            }
        }
    }
}
