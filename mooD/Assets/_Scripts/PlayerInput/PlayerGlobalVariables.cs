﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(CapsuleCollider))]

public class PlayerGlobalVariables : MonoBehaviour
{
    //Movement variables
    public float movementSpeed = 40;
    public float lookSpeed = 5;

    //Should the player move?
    public bool playerShouldMove = true;

    //Detect whether the camera should fade
    public bool shouldFade = false;
    
    //Create texture to paint screen
    private Texture2D _blackScreen;

    //Use opacity for fading
    public float opacity = 1;

    //The player's UI variables
    public int
        PlayerHP = 100,
        Bullets = 30,
        HighScore = 0;

    //The player's firing rate
    public float FireRate = 0.5f;

    //The UI
    private UserInterface _userInterface;

    private void Start()
    {
        //Instance creation
        _userInterface = GameObject.FindGameObjectWithTag("PlayerManager").GetComponent<UserInterface>();

        //Create the square texture with 1x1 size for fading
        _blackScreen = new Texture2D(1, 1);
        _blackScreen.SetPixel(0, 0, new Color(0, 0, 0, opacity));
        _blackScreen.Apply();
    }

    private void Update()
    {
        //Clamp the HP and bullet values
        PlayerHP = Mathf.Clamp(PlayerHP, 0, 100);
        Bullets = Mathf.Clamp(Bullets, 0, 99);

        //Fade cycles
        if (!shouldFade)
        {
            if (opacity > 0)
            {
                //Gradually decrease opacity
                opacity -= Time.deltaTime * .3f;
                //Cap opacity at 0
                if (opacity < 0)
                {
                    opacity = 0;
                }
                _blackScreen.SetPixel(0, 0, new Color(0, 0, 0, opacity));
                _blackScreen.Apply();
            }
        }
        else
        {
            if (opacity < 1)
            {
                //Gradually increase opacity
                opacity += Time.deltaTime * .3f;
                //Cap opacity at 1
                if (opacity > 1)
                {
                    opacity = 1;
                }
                _blackScreen.SetPixel(0, 0, new Color(0, 0, 0, opacity));
                _blackScreen.Apply();
            }
        }
    }

    private void OnGUI()
    {
        //Guarantee the black screen fade is behind the dialogue
        GUI.depth = 100;
        GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), _blackScreen);
    }

    public void DelayedFadeToggle()
    {
        Invoke("FadeToggle", 2f);
    }

    public void FadeToggle()
    {
        shouldFade = !shouldFade;
    }

    public void FullTransparency()
    {
        if (opacity > 0)
        {
            //Almost instantly decrease opacity
            opacity -= Time.deltaTime * 5f;
            //Cap opacity at 0
            if (opacity < 0)
            {
                opacity = 0;
            }
            _blackScreen.SetPixel(0, 0, new Color(0, 0, 0, opacity));
            _blackScreen.Apply();
            shouldFade = false;
        }
    }

    public void FullOpacity()
    {
        if (opacity < 1)
        {
            //Almost instantly decrease opacity
            opacity += Time.deltaTime * 5f;
            //Cap opacity at 1
            if (opacity < 1)
            {
                opacity = 1;
            }
            _blackScreen.SetPixel(0, 0, new Color(0, 0, 0, opacity));
            _blackScreen.Apply();
            shouldFade = true;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        //Reset rotation and velocity when colliding with a wall to prevent rolling
        if (collision.gameObject.name == "Wall")
        {
            GetComponent<Rigidbody>().velocity = Vector3.zero;
        }
        else if (collision.gameObject.name == "Door")
        {
            GetComponent<Rigidbody>().velocity = Vector3.zero;
        }
        else if (collision.gameObject.name == "SpawnDoor")
        {
            GetComponent<Rigidbody>().velocity = Vector3.zero;
        }
        else if (collision.gameObject.name == "Cover")
        {
            GetComponent<Rigidbody>().velocity = Vector3.zero;
        }
    }

    public void DelayedToggleMovement()
    {
        Invoke("ToggleMovement", 2f);
    }

    public void ToggleMovement()
    {
        playerShouldMove = !playerShouldMove;
    }

    public void ReceiveBaseDamage()
    {
        //Receive damage
        PlayerHP -= 10;

        //Check if the character icon should change due to health loss
        _userInterface.CheckIfChangeIcon();
    }
}
