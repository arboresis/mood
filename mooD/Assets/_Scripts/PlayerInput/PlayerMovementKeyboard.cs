﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementKeyboard : MonoBehaviour
{
    //The player character's global variables
    private PlayerGlobalVariables _globalVariables;

    // Start is called before the first frame update
    void Start()
    {
        //Instance creation
        _globalVariables = GetComponent<PlayerGlobalVariables>();
    }

    // Update is called once per frame
    void Update()
    {
        if (_globalVariables.playerShouldMove)
        {
            //Calculate the input
            var verticalMovement = Input.GetAxisRaw("Vertical") * _globalVariables.movementSpeed;
            var horizontalMovement = Input.GetAxisRaw("Horizontal") * _globalVariables.movementSpeed;

            //Adjust the input to match time
            verticalMovement *= Time.deltaTime;
            horizontalMovement *= Time.deltaTime;

            //Translate according to the player input
            transform.Translate(horizontalMovement, 0, verticalMovement);
        }
    }
}
