﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementMouse : MonoBehaviour
{
    //The player character's global variables
    private PlayerGlobalVariables _globalVariables;

    // Start is called before the first frame update
    void Start()
    {
        //Instance creation
        _globalVariables = GetComponent<PlayerGlobalVariables>();
    }

    // Update is called once per frame
    void Update()
    {
        if (_globalVariables.playerShouldMove)
        {
            //Calculate the input
            var verticalMouse = Mathf.Clamp(Input.GetAxisRaw("Mouse Y"), -3f, 3f) * _globalVariables.movementSpeed * 0.5f;
            var horizontalMouse = Input.GetAxisRaw("Mouse X") * _globalVariables.lookSpeed * 5;

            //Adjust the input to match time
            verticalMouse *= Time.deltaTime;
            horizontalMouse *= Time.deltaTime;

            //Move according to the mouse's yy input
            transform.Translate(0, 0, verticalMouse);
            //Rotate according to the mouse's xx input
            transform.Rotate(0, horizontalMouse, 0);
        }
    }
}
