﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInteractionHandler : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        //Interaction handle
        if (Input.GetKeyDown(KeyCode.F) || Input.GetMouseButtonDown(1))
        {
            Interaction();
        }
    }

    void Interaction()
    {
        //The target object
        RaycastHit otherGO;
        //The maximum ray distance
        float maxRayDistance = 1.8f;

        //Raycast from the player's position to a point 1.8 units forward
        Physics.Raycast(new Ray(transform.position, transform.forward), out otherGO, maxRayDistance);

        //Send interaction message to the other GameObject
        if (otherGO.collider != null)
        {
            otherGO.collider.SendMessage("Interaction", SendMessageOptions.DontRequireReceiver);
        }
    }
}
