﻿using System.Collections;
using UnityEngine;

public class FakeWallMovement : MonoBehaviour
{
    //Check if this fake wall was already interacted with
    private bool _interacted = false;

    public void Interaction()
    {
        //If it wasn't interacted with, move this fake wall back
        if(_interacted == false)
        {
            StartCoroutine("MoveWallBack");
        }
        //Set the interaction check as true
        _interacted = true;
    }

    IEnumerator MoveWallBack()
    {
        int i = 0;

        //Move 0.05 units back fourty times
        while(i < 40)
        {
            transform.position -= transform.parent.forward * - 0.05f;
            i++;
            yield return new WaitForSeconds(0.05f);
        }
    }
}
