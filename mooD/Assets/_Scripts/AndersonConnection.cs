﻿using System.Collections.Generic;
using UnityEngine;

public class AndersonConnection : MonoBehaviour
{
    //A list with all the Old Men Anderson
    public List<GameObject> OldManList = new List<GameObject>();

    // Start is called before the first frame update
    void Awake()
    {
        //Instance creation
        GameObject[] TempOldManList = GameObject.FindGameObjectsWithTag("OldManEnemy");
        //Fill the list with every old man
        foreach(GameObject OldMan in TempOldManList)
        {
            OldManList.Add(OldMan);
        }
    }
}
