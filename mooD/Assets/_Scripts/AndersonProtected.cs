﻿using UnityEngine;

public class AndersonProtected : MonoBehaviour
{
    //Check if this enemy is being protected
    public bool IsProtected = false;
    //The enemy protecting it
    public GameObject Protector;

    // Start is called before the first frame update
    void Start()
    {
        //Reset values
        IsProtected = false;
    }
}
