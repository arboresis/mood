﻿using System.Collections;
using UnityEngine;

public class DoorInteractionScript : MonoBehaviour
{
    //Define time start and duration of the animation
    public float timeCount = 0f;
    public float duration = 1.1f;

    //Avoid action overlap
    private bool _isMoving = false;
    private bool _isOpening = false;

    //The target positions
    private Vector3
        _initialPosition = new Vector3(0, 0.5f, 0),
        _finalPosition = new Vector3(0, -0.6f, 0);
    public Vector3 _doorPosition;

    public float distance;

    //The player
    private GameObject Player;

    private void Start()
    {
        //Instance creation
        _doorPosition = transform.position;
        Player = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update ()
    {
        distance = Vector3.Distance(_doorPosition, Player.transform.position);

        if (_isMoving)
        {
            Movement();
        }
    }

    public void Interaction()
    {
        _isOpening = !_isOpening;
        _isMoving = true;

        StartCoroutine(IsPlayerHere());
    }

    public void Movement()
    {
        if(_isOpening)
        {
            transform.localPosition = Vector3.Lerp(_initialPosition, _finalPosition, timeCount / duration);
        }
        else
        {
            transform.localPosition = Vector3.Lerp(_finalPosition, _initialPosition, timeCount / duration);
        }

        timeCount = timeCount + Time.deltaTime;

        if (timeCount >= duration)
        {
            _isMoving = false;
            timeCount = 0;
        }
    }

    private IEnumerator IsPlayerHere()
    {
        yield return new WaitForSeconds(1f);
        if(distance > 1f)
        {
            _isOpening = false;
            _isMoving = true;
            Movement();
        }
        else
        {
            StartCoroutine(IsPlayerHere());
        }
    }
}
