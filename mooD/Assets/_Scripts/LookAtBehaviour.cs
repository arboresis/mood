﻿using UnityEngine;

public class LookAtBehaviour : MonoBehaviour
{
    //The player's Transform
    protected Transform _playerTransform;

    private void Start()
    {
        //Instance creation
        _playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
    }

    void Update ()
    {
        //General LookAt
        transform.LookAt(_playerTransform.position);

        //Lock LookAt to one axis by grounding the other two
        transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, 0);
	}
}
