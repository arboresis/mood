﻿using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public class AndersonProtector : MonoBehaviour
{
    //This unit's NavMeshAgent
    private NavMeshAgent navMeshAgent;

    //This unit's protection target
    public GameObject ProtectionTarget;

    //The Old Man Anderson list script
    AndersonConnection andersonConnection;

    // Start is called before the first frame update
    void Start()
    {
        //Instance creation
        navMeshAgent = GetComponent<NavMeshAgent>();
        andersonConnection = GameObject.FindGameObjectWithTag("EnemyManager").GetComponent<AndersonConnection>();

        //Start looking for a protection target
        StartCoroutine(LookForProtectionTarget());
    }

    IEnumerator LookForProtectionTarget()
    {
        yield return new WaitForSeconds(0.5f);
        
        foreach(GameObject OldMan in andersonConnection.OldManList)
        {
            //If the old man's protector is null
            if (ProtectionTarget == null)
            {
                //If the old man is not protected
                if (!OldMan.GetComponent<AndersonProtected>().IsProtected)
                {
                    //Set this unit's protection target as this old man
                    ProtectionTarget = OldMan;
                    //Set the old man's protector as this unit
                    OldMan.GetComponent<AndersonProtected>().IsProtected = true;
                    OldMan.GetComponent<AndersonProtected>().Protector = gameObject;
                    GetComponent<Animator>().SetBool("ShouldUse", false);

                    //Start protecting the target
                    StartCoroutine("Protect");
                }
            }
        }

        //If there is still no protection target, try again in 0.5 seconds
        if (ProtectionTarget == null)
        {
            StartCoroutine(LookForProtectionTarget());
        }
    }

    IEnumerator Protect()
    {
        //While this unit is active
        while (isActiveAndEnabled)
        {
            if (ProtectionTarget != null)
            {
                //navMeshAgent.destination = ProtectionTarget.transform.position + Vector3.forward * 0.1f;
            }
            yield return new WaitForSeconds(0.1f);
        }
    }
}
