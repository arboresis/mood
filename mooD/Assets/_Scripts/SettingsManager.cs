﻿using UnityEngine;

public class SettingsManager : MonoBehaviour
{
    //The maximum target fps rate
    public int MaximumFramerate = 20;

    // Start is called before the first frame update
    void Start()
    {
        //Set the quality settings
        QualitySettings.maximumLODLevel = 0;
        QualitySettings.antiAliasing = 0;
        QualitySettings.shadows = ShadowQuality.Disable;
        QualitySettings.vSyncCount = 0;
        Screen.SetResolution(320, 200, true);
        Application.targetFrameRate = MaximumFramerate;

        //Hide mouse cursor
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }
}
