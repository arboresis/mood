﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConstantRotation : MonoBehaviour
{
    //The target position and rotation values
    Vector3
        _rotation = new Vector3(0, 10f, 0),
        _position = new Vector3(0, 0.01f, 0);

    // Start is called before the first frame update
    void Start()
    {
        //Start rotating horizontally and moving vertically
        StartCoroutine("HorizontalRotation");
        StartCoroutine("VerticalMovement");

        //Repeatedly invoke the direction changer
        InvokeRepeating("ChangeDirection", 1, 1);
    }

    private void ChangeDirection()
    {
        //Invert the vector
        _position *= -1;
    }

    IEnumerator HorizontalRotation()
    {
        //Endlessly rotate
        while (isActiveAndEnabled)
        {
            transform.Rotate(_rotation);
            yield return new WaitForSeconds(0.1f);
        }
    }

    IEnumerator VerticalMovement()
    {
        //Endlessly move
        while (isActiveAndEnabled)
        {
            transform.Translate(_position);
            yield return new WaitForSeconds(0.01f);
        }
    }
}
